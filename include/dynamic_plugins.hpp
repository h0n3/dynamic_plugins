//
// Created by h on 29.11.19.
//

#ifndef DYNAMIC_PLUGINS_HPP
#define DYNAMIC_PLUGINS_HPP

#include <tuple>
#include <boost/dll.hpp>

#include "dynamic_plugins/function.hpp"
#include "dynamic_plugins/interface.hpp"

#endif // DYNAMIC_PLUGINS_HPP
