//
// Created by h on 30.11.19.
//

#ifndef DYNAMIC_PLUGINS_FUNCTION_HPP
#define DYNAMIC_PLUGINS_FUNCTION_HPP

namespace dynamic_plugins {

#define DEFINE_PLUGIN_INTERFACE_FUNCTION(NAME, LIB_NAME, RETURN_TYPE, ARGS...) \
    constexpr const char* NAME##_name()                                        \
    {                                                                          \
        return LIB_NAME;                                                       \
    }                                                                          \
    using NAME = plugin_interface_function<&NAME##_name, RETURN_TYPE, ARGS>

using name_func_t = const char* (*)();

template <name_func_t __name_func, class __return_t, class... __args_t>
class function {
public:
    using return_t    = __return_t;
    using prototype_t = return_t (*)(__args_t...);

private:
    prototype_t function;

public:
    void load(boost::dll::shared_library& lib)
    {
        const char* name{ __name_func() };

        if (!lib.has(name))
            throw "sth";

        function = lib.get<prototype_t>(name);
    }

    return_t operator()(__args_t... args)
    {
        return function(args...);
    }
};

}

#endif // DYNAMIC_PLUGINS_FUNCTION_HPP
