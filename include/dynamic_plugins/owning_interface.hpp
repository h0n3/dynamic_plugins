//
// Created by h on 30.11.19.
//

#ifndef DYNAMIC_PLUGINS_OWNING_INTERFACE_HPP
#define DYNAMIC_PLUGINS_OWNING_INTERFACE_HPP

namespace dynamic_plugins {

template <class parent_t>
class owning_interface : public parent_t {
    boost::dll::shared_library _shared_lib;

public:
    owning_interface() = default;

    owning_interface(const char* path)
        : _shared_lib{ path }
    {
        parent_t::load(_shared_lib);
    }

    void load(const char* path)
    {
        _shared_lib.load(path);
        parent_t::load(_shared_lib);
    }
};
} // namespace dynamic_plugins

#endif // DYNAMIC_PLUGINS_OWNING_INTERFACE_HPP
