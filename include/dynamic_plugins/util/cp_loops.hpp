//
// Created by h on 30.11.19.
//

#ifndef DYNAMIC_PLUGINS_CP_LOOPS_HPP
#define DYNAMIC_PLUGINS_CP_LOOPS_HPP

#include <iostream>
#include <tuple>
#include <functional>

/*
 *   Library Code
 */

template <class functor_t, class member_return_t, class... args_t>
void apply_member_func(functor_t& functor,
    member_return_t (functor_t::*func)(args_t...),
    std::tuple<args_t...>& data)
{
    std::apply([&functor, func](args_t... args) { (functor.*func)(args...); }, data);
}

template <class resolver_t, class data_t, class... functors_t>
void ct_for_each(data_t data, functors_t&... functors)
{
    (apply_member_func(functors, resolver_t::resolve(functors), data), ...);
}

template <class return_t, class first_t, class... args_t>
constexpr auto bind_first_func(first_t& first, return_t (*func)(first_t&, args_t...))
{
    return [&first, func](args_t... args) { return func(first, args...); };
}

template <class resolver_t, class data_t, class... functors_t>
struct ct_for_each_tuple_iml {
    using this_t = ct_for_each_tuple_iml<resolver_t, data_t, functors_t...>;

    static inline void for_each(data_t& data, functors_t&... functors)
    {
        ct_for_each<resolver_t, data_t, functors_t&...>(data, functors...);
    }

    static inline void eval(data_t& data, std::tuple<functors_t...>& functors)
    {
        std::apply(bind_first_func(data, &this_t::for_each), functors);
    }
};

template <class resolver_t, class data_t, class... functors_t>
void ct_for_each_tuple(data_t& data, std::tuple<functors_t...>& functors)
{
    ct_for_each_tuple_iml<resolver_t, data_t, functors_t...>::eval(data, functors);
}

#define DEFINE_FUNCTION_RESOLVER(NAME, FUNCTION_NAME)     \
    struct NAME {                                         \
        template <class functor_t>                        \
        static constexpr auto resolve(functor_t& functor) \
        {                                                 \
            return &functor_t::FUNCTION_NAME;             \
        }                                                 \
    }

// DEFINE_FUNCTION(load, boost::dll::shared_library &)
// Even though I usually dont like macros too much this one makes sense since it reduces the amount of
// repetition
#define DEFINE_FUNCTION(FUNCTION_NAME, ARGUMENT_TYPES...)                                   \
    DEFINE_FUNCTION_RESOLVER(FUNCTION_NAME##_resolver, FUNCTION_NAME);                      \
                                                                                            \
    template <class data_t, class... functors_t>                                            \
    void call_##FUNCTION_NAME(data_t& data, std::tuple<functors_t...>& functors)            \
    {                                                                                       \
        ct_for_each_tuple<FUNCTION_NAME##_resolver, data_t, functors_t...>(data, functors); \
    }

#endif // DYNAMIC_PLUGINS_CP_LOOPS_HPP
