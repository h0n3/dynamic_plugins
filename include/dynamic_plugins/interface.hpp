//
// Created by h on 30.11.19.
//

#ifndef DYNAMIC_PLUGINS_INTERFACE_HPP
#define DYNAMIC_PLUGINS_INTERFACE_HPP

#include "util/cp_loops.hpp"

#define DEFINE_PLUGIN_INTERFACE(NAME, FUNCTIONS...) using NAME = dynamic_plugins::interface<FUNCTIONS>

namespace dynamic_plugins {

template <class... functions_t>
class interface {
    std::tuple<functions_t...> _functions;

    DEFINE_FUNCTION(load, boost::dll::shared_library&)
public:
    interface() = default;

    interface(boost::dll::shared_library& lib)
    {
        __load(lib);
    }

    void load(boost::dll::shared_library& lib)
    {
        __load(lib);
    }

    template <class function_t>
    constexpr auto get()
    {
        return std::get<function_t>(_functions);
    }

    template <class function_t, class... args_t>
    constexpr auto call(args_t... args)
    {
        auto func = std::get<function_t>(_functions);
        return func(args...);
    }

private:
    void __load(boost::dll::shared_library& lib)
    {
        std::tuple<boost::dll::shared_library&> args(lib);
        call_load(args, _functions);
    }
};
}

#endif // DYNAMIC_PLUGINS_INTERFACE_HPP
