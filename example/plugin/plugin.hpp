//
// Created by h on 29.11.19.
//

#ifndef DYNAMIC_PLUGINS_EXAMPLE_PLUGIN_HPP
#define DYNAMIC_PLUGINS_EXAMPLE_PLUGIN_HPP

#include <iostream>
#include "interface.hpp"

class plugin : public test_interface {
    int _val;

public:
    plugin(int val)
        : _val{ val }
    {
    }

    virtual ~plugin() = default;

    virtual void interface_func() override
    {
        std::cout << "plugin::interface_func()" << std::endl;
    }

    static test_interface* fac_func(int val)
    {
        return new plugin(val);
    }      
};

void func2()
{
    std::cout << "func2()" << std::endl;
}

DEFINE_TEST_INTERFACE_PLUGIN_IMPLEMENTATION(plugin::fac_func, func2)

#endif // DYNAMIC_PLUGINS_EXAMPLE_PLUGIN_HPP
