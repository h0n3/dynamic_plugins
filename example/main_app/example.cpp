//
// Created by h on 29.11.19.
//
#include "interface.hpp"
#include "dynamic_plugins/owning_interface.hpp"

using intf_t = dp::owning_interface<test_plugin_interface_t>;

int main(int argc, char** argv)
{
    intf_t interface{ argv[1] };

    auto            func = interface.get<test_func_t>();
    test_interface* obj  = func(42);
    obj->interface_func();
    delete obj;

    obj = interface.call<test_func_t>(42);
    obj->interface_func();
    delete obj;

    interface.call<test_func2_t>();
}