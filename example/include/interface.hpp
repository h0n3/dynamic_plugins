//
// Created by h on 30.11.19.
//

#ifndef DYNAMIC_PLUGINS_EXAMPLE_INTERFACE_HPP
#define DYNAMIC_PLUGINS_EXAMPLE_INTERFACE_HPP

#include "dynamic_plugins.hpp"

namespace dp = ::dynamic_plugins;

class test_interface {
public:
    virtual ~test_interface() = default;

    virtual void interface_func() = 0;
};

constexpr const char* test_func_name()
{
    return "test";
}
using test_func_t = dp::function<test_func_name, test_interface*, int>;
constexpr const char* test2_func_name()
{
    return "test2";
}
using test_func2_t = dp::function<test2_func_name, void>;

DEFINE_PLUGIN_INTERFACE(test_plugin_interface_t, test_func_t, test_func2_t);

#define DEFINE_TEST_INTERFACE_PLUGIN_IMPLEMENTATION(TEST_FUNC_IMPL, TEST_FUNC2_IMPL) \
    BOOST_DLL_ALIAS(TEST_FUNC_IMPL, test)                                            \
    BOOST_DLL_ALIAS(TEST_FUNC2_IMPL, test2)

#endif // DYNAMIC_PLUGINS_EXAMPLE_INTERFACE_HPP
